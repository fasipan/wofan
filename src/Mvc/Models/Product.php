<?php
namespace WFan\Mvc\Models;

/**
 * Product
 */
class Product extends \WC_Product
{

    public $id;

    public $title;

    public $name;

    public $content;

    public $excerpt;

    public $thumbnail;

    public $gallery;

    public $files;

    public $link;

    public $sku;

    public $price;

    public $ratting;

    public $review;

    public $catalogues;

    public $tags;

    public $html;

    public function __construct($product = null)
    {
        if (!is_object($product)) {
            return;
        }
        parent::__construct($product);

        $images = [];
        foreach ($this->get_gallery_attachment_ids() as $key => $attachment_id) {
            $images[] = new \Timber\Image($attachment_id);
        }
        $this->gallery = $images;

        $this->link = $this->get_permalink();

        $this->title = $this->get_title();

        $this->sku = $this->get_sku();

        if ($this->get_files()) {
            $this->files = array_values(array_map(function ($item) {
                return $item;
            }, $this->get_files()));
        }
        $this->price = array(
            "price"               => $this->get_price(),
            "price_sale"          => $this->get_sale_price(),
            "price_regular"       => $this->get_regular_price(),
            "price_excluding_tax" => $this->get_price_excluding_tax(),
            "price_including_tax" => $this->get_price_including_tax(),
            "price_text"          => ($this->get_regular_price() > 0) ? wc_price($this->get_price_including_tax(1, $this->get_sale_price())) : __("Contact", "nubade"),
        );

        $this->ratting = array(
            "rating_count"   => $this->get_rating_count(),
            "rating_average" => $this->get_average_rating(),
        );

        $this->review = $this->get_review_count();

        $this->thumbnail = new \Timber\Image($this->get_image_id());
        $catalogues      = get_the_terms($this->id, "product_cat");
        if ($catalogues) {
            foreach ($catalogues as $key => $catalogue) {
                $catalogue->link    = esc_url(get_term_link($catalogue, "product_cat"));
                $this->catalogues[] = $catalogue;
            }
        }
        $tags = get_the_terms($this->id, "product_tag");
        if ($tags) {
            foreach ($tags as $key => $tag) {
                $tag->link    = esc_url(get_term_link($tag, "product_tag"));
                $this->tags[] = $tag;
            }
        }

    }

    public function getGallery()
    {
        return $this->gallery;
    }

    public function getLink()
    {
        return $this->link;
    }

    public function __call($name, array $args)
    {
        if ($this->get_attribute($name)) {
            return $this->get_attribute($name);
        }
        dd("not property");
    }

}
