<?php
namespace WFan\Mvc\Models;

class Term extends \Timber\Term
{
    const SLUG_NAME = "_thumbnail_id";

    public $term_id;

    public $term_taxonomy_id;

    public $slug;

    // public function __call($method, $args)
    // {
    //     //dd($method);
    // }

    public function __get($name)
    {
        if (!property_exists(get_class(), $name)) {
            return \Meta::term($this->term_taxonomy_id, $name);

        }
    }
}
