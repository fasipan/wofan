<?php
namespace WebQuick\Models;

/**
 * Ecommerce
 */
class ECommerce
{
    const posts_per_page = "10";
    public function __construct()
    {
        dd("sdfsd");
        $this->render();
    }
    public static function getProducts($type = "latest")
    {
        $products = [];
        $params   = self::getParamsProduct();

        $loop = new \WP_Query($params);
        if ($loop->have_posts()) {
            while ($loop->have_posts()): $loop->the_post();
                global $post, $product;
                $id_thumb   = get_post_thumbnail_id();
                $products[] = new Product([
                    "id_product"       => $post->ID,
                    "lb_name"          => get_the_title(),
                    "lb_img_thumbnail" => wp_get_attachment_image_url($id_thumb, 'thumbnail'),
                    "lb_img_medium"    => wp_get_attachment_image_url($id_thumb, 'medium'),
                    "lb_img_full"      => wp_get_attachment_image_url($id_thumb, 'full'),
                    "lb_link"          => get_permalink(),
                    "lb_photos"        => "",
                    "lb_price"         => $product->get_price(),
                ]);
            endwhile;
            wp_reset_postdata();
        }
        return $products;
    }
    public function getProduct()
    {

    }
    public function getCategories()
    {

    }
    public function getCategory()
    {

    }

    public function render()
    {

        function woocommerce_show_product_thumbnails()
        {
            echo "apple";
        }
    }

    public static function getParamsProduct($params = null, $type = "latest")
    {
        $common_args = array(
            'post_type'      => 'product',
            'posts_per_page' => self::posts_per_page,
            'post_status'    => 'publish',
            'meta_query'     => array(
                array(
                    'key'     => '_stock_status',
                    'value'   => 'outofstock',
                    'compare' => 'NOT IN',
                ),
            ),
        );
        $args = [];
        switch ($type) {
            case 'older':
                $older_args = array(
                    'orderby' => 'date',
                    'order'   => 'ASC',
                );
                $args = array_merge($common_args, $older_args);
                break;
            case 'featured':
                $featured_args = array(
                    'meta_key'   => '_featured',
                    'meta_value' => 'yes',
                );
                $args = array_merge($common_args, $featured_args);
                break;
            case 'category':
                $category_args = array(
                    'tax_query' => array(
                        'relation' => 'AND',
                        array(
                            'taxonomy' => 'product_cat',
                            'field'    => 'slug',
                            'terms'    => $params["category_slug"],
                        ),
                    ),
                );
                $args = array_merge($common_args, $featured_args);
                break;
            default:
                $args = $common_args;
                break;
        }
        return $args;
    }

}
