<?php
namespace WFan\MVc\Models;

/**
 * Post
 */
class Post
{
    public $attrs = array(
        'author'               => '',
        'category'             => '',
        'category_display'     => '',
        'category_label'       => 'Posted in: ',
        'content_class'        => 'content',
        'date_format'          => '(n/j/Y)',
        'date'                 => '',
        'date_column'          => 'post_date',
        'date_compare'         => '=',
        'date_query_before'    => '',
        'date_query_after'     => '',
        'date_query_column'    => '',
        'date_query_compare'   => '',
        'display_posts_off'    => false,
        'excerpt_length'       => false,
        'excerpt_more'         => false,
        'excerpt_more_link'    => false,
        'exclude_current'      => false,
        'id'                   => false,
        'ignore_sticky_posts'  => false,
        'image_size'           => false,
        'include_author'       => false,
        'include_content'      => false,
        'include_date'         => false,
        'include_excerpt'      => false,
        'include_title'        => true,
        'meta_key'             => '',
        'meta_value'           => '',
        'no_posts_message'     => '',
        'offset'               => 0,
        'order'                => 'DESC',
        'orderby'              => 'date',
        'post_parent'          => false,
        'post_status'          => 'publish',
        'post_type'            => 'post',
        'posts_per_page'       => '10',
        'tag'                  => '',
        'tax_operator'         => 'IN',
        'tax_include_children' => true,
        'tax_term'             => false,
        'taxonomy'             => false,
        'time'                 => '',
        'title'                => '',
        'wrapper'              => 'ul',
        'wrapper_class'        => 'display-posts-listing',
        'wrapper_id'           => false,
        'display'              => "list",
    );

    public function __construct()
    {

    }

    public function addShortcode()
    {
        add_shortcode('posts', function ($attrs) {
            $display   = sanitize_text_field($attrs['display']);
            $display   = $display ?: "list";
            $attrs     = shortcode_atts($this->attrs, $attrs, 'posts');
            $posts     = $this->_getPosts($this->_getParams($attrs));
            $htmlItems = "";

            ob_start();
            foreach ($posts as $key => $post) {
                \Timber\Timber::render(["components/item/$display-post-item.twig", "components/item/$display-post-item.twig"], ["post" => $post]);
            }
            $htmlItems = ob_get_contents();
            ob_clean();
            $html = "";
            ob_start();
            \Timber\Timber::render(["components/$display/post-$id.twig", "components/$display/post.twig"], ["posts" => $htmlItems]);
            $html = ob_get_clean();
            return $html;
        });
    }

    public function getPosts($params = [])
    {
        return \View::get_posts($params);
    }

    protected function _getPost()
    {

    }

    protected function _getCategory()
    {

    }

    public static function getParams($atts)
    {
        $original_atts        = $atts;
        $author               = sanitize_text_field($atts['author']);
        $category             = sanitize_text_field($atts['category']);
        $category_display     = 'true' == $atts['category_display'] ? 'category' : sanitize_text_field($atts['category_display']);
        $category_label       = sanitize_text_field($atts['category_label']);
        $content_class        = array_map('sanitize_html_class', (explode(' ', $atts['content_class'])));
        $date_format          = sanitize_text_field($atts['date_format']);
        $date                 = sanitize_text_field($atts['date']);
        $date_column          = sanitize_text_field($atts['date_column']);
        $date_compare         = sanitize_text_field($atts['date_compare']);
        $date_query_before    = sanitize_text_field($atts['date_query_before']);
        $date_query_after     = sanitize_text_field($atts['date_query_after']);
        $date_query_column    = sanitize_text_field($atts['date_query_column']);
        $date_query_compare   = sanitize_text_field($atts['date_query_compare']);
        $excerpt_length       = intval($atts['excerpt_length']);
        $excerpt_more         = sanitize_text_field($atts['excerpt_more']);
        $excerpt_more_link    = filter_var($atts['excerpt_more_link'], FILTER_VALIDATE_BOOLEAN);
        $exclude_current      = filter_var($atts['exclude_current'], FILTER_VALIDATE_BOOLEAN);
        $id                   = $atts['id']; // Sanitized later as an array of integers
        $ignore_sticky_posts  = filter_var($atts['ignore_sticky_posts'], FILTER_VALIDATE_BOOLEAN);
        $image_size           = sanitize_key($atts['image_size']);
        $include_title        = filter_var($atts['include_title'], FILTER_VALIDATE_BOOLEAN);
        $include_author       = filter_var($atts['include_author'], FILTER_VALIDATE_BOOLEAN);
        $include_content      = filter_var($atts['include_content'], FILTER_VALIDATE_BOOLEAN);
        $include_date         = filter_var($atts['include_date'], FILTER_VALIDATE_BOOLEAN);
        $include_excerpt      = filter_var($atts['include_excerpt'], FILTER_VALIDATE_BOOLEAN);
        $meta_key             = sanitize_text_field($atts['meta_key']);
        $meta_value           = sanitize_text_field($atts['meta_value']);
        $no_posts_message     = sanitize_text_field($atts['no_posts_message']);
        $offset               = intval($atts['offset']);
        $order                = sanitize_key($atts['order']);
        $orderby              = sanitize_key($atts['orderby']);
        $post_parent          = $atts['post_parent']; // Validated later, after check for 'current'
        $post_status          = $atts['post_status']; // Validated later as one of a few values
        $post_type            = sanitize_text_field($atts['post_type']);
        $posts_per_page       = intval($atts['posts_per_page']);
        $tag                  = sanitize_text_field($atts['tag']);
        $tax_operator         = $atts['tax_operator']; // Validated later as one of a few values
        $tax_include_children = filter_var($atts['tax_include_children'], FILTER_VALIDATE_BOOLEAN);
        $tax_term             = sanitize_text_field($atts['tax_term']);
        $taxonomy             = sanitize_key($atts['taxonomy']);
        $time                 = sanitize_text_field($atts['time']);
        $shortcode_title      = sanitize_text_field($atts['title']);
        $wrapper              = sanitize_text_field($atts['wrapper']);
        $wrapper_class        = array_map('sanitize_html_class', (explode(' ', $atts['wrapper_class'])));

        if (!empty($wrapper_class)) {
            $wrapper_class = ' class="' . implode(' ', $wrapper_class) . '"';
        }

        $wrapper_id = sanitize_html_class($atts['wrapper_id']);
        if (!empty($wrapper_id)) {
            $wrapper_id = ' id="' . $wrapper_id . '"';
        }

        /**
         * Twig
         */
        $display = sanitize_text_field($atts['display']);

        // Set up initial query for post
        $args = array(
            'category_name'  => $category,
            'order'          => $order,
            'orderby'        => $orderby,
            'post_type'      => explode(',', $post_type),
            'posts_per_page' => $posts_per_page,
            'tag'            => $tag,
        );

        // Date query.
        if (!empty($date) || !empty($time) || !empty($date_query_after) || !empty($date_query_before)) {
            $initial_date_query = $date_query_top_lvl = array();

            $valid_date_columns = array(
                'post_date', 'post_date_gmt', 'post_modified', 'post_modified_gmt',
                'comment_date', 'comment_date_gmt',
            );

            $valid_compare_ops = array('=', '!=', '>', '>=', '<', '<=', 'IN', 'NOT IN', 'BETWEEN', 'NOT BETWEEN');

            // Sanitize and add date segments.
            $dates = be_sanitize_date_time($date);
            if (!empty($dates)) {
                if (is_string($dates)) {
                    $timestamp = strtotime($dates);
                    $dates     = array(
                        'year'  => date('Y', $timestamp),
                        'month' => date('m', $timestamp),
                        'day'   => date('d', $timestamp),
                    );
                }
                foreach ($dates as $arg => $segment) {
                    $initial_date_query[$arg] = $segment;
                }
            }

            // Sanitize and add time segments.
            $times = be_sanitize_date_time($time, 'time');
            if (!empty($times)) {
                foreach ($times as $arg => $segment) {
                    $initial_date_query[$arg] = $segment;
                }
            }

            // Date query 'before' argument.
            $before = be_sanitize_date_time($date_query_before, 'date', true);
            if (!empty($before)) {
                $initial_date_query['before'] = $before;
            }

            // Date query 'after' argument.
            $after = be_sanitize_date_time($date_query_after, 'date', true);
            if (!empty($after)) {
                $initial_date_query['after'] = $after;
            }

            // Date query 'column' argument.
            if (!empty($date_query_column) && in_array($date_query_column, $valid_date_columns)) {
                $initial_date_query['column'] = $date_query_column;
            }

            // Date query 'compare' argument.
            if (!empty($date_query_compare) && in_array($date_query_compare, $valid_compare_ops)) {
                $initial_date_query['compare'] = $date_query_compare;
            }

            //
            // Top-level date_query arguments. Only valid arguments will be added.
            //

            // 'column' argument.
            if (!empty($date_column) && in_array($date_column, $valid_date_columns)) {
                $date_query_top_lvl['column'] = $date_column;
            }

            // 'compare' argument.
            if (!empty($date_compare) && in_array($date_compare, $valid_compare_ops)) {
                $date_query_top_lvl['compare'] = $date_compare;
            }

            // Bring in the initial date query.
            if (!empty($initial_date_query)) {
                $date_query_top_lvl[] = $initial_date_query;
            }

            // Date queries.
            $args['date_query'] = $date_query_top_lvl;
        }

        // Ignore Sticky Posts
        if ($ignore_sticky_posts) {
            $args['ignore_sticky_posts'] = true;
        }

        // Meta key (for ordering)
        if (!empty($meta_key)) {
            $args['meta_key'] = $meta_key;
        }

        // Meta value (for simple meta queries)
        if (!empty($meta_value)) {
            $args['meta_value'] = $meta_value;
        }

        // If Post IDs
        if ($id) {
            $posts_in         = array_map('intval', explode(',', $id));
            $args['post__in'] = $posts_in;
        }

        // If Exclude Current
        if (is_singular() && $exclude_current) {
            $args['post__not_in'] = array(get_the_ID());
        }

        // Post Author
        if (!empty($author)) {
            if ('current' == $author && is_user_logged_in()) {
                $args['author_name'] = wp_get_current_user()->user_login;
            } elseif ('current' == $author) {
                $args['meta_key'] = 'dps_no_results';
            } else {
                $args['author_name'] = $author;
            }

        }

        // Offset
        if (!empty($offset)) {
            $args['offset'] = $offset;
        }

        // Post Status
        $post_status = explode(', ', $post_status);
        $validated   = array();
        $available   = array('publish', 'pending', 'draft', 'auto-draft', 'future', 'private', 'inherit', 'trash', 'any');
        foreach ($post_status as $unvalidated) {
            if (in_array($unvalidated, $available)) {
                $validated[] = $unvalidated;
            }
        }

        if (!empty($validated)) {
            $args['post_status'] = $validated;
        }

        // If taxonomy attributes, create a taxonomy query
        if (!empty($taxonomy) && !empty($tax_term)) {

            if ('current' == $tax_term) {
                global $post;
                $terms    = wp_get_post_terms(get_the_ID(), $taxonomy);
                $tax_term = array();
                foreach ($terms as $term) {
                    $tax_term[] = $term->slug;
                }
            } else {
                // Term string to array
                $tax_term = explode(', ', $tax_term);
            }

            // Validate operator
            if (!in_array($tax_operator, array('IN', 'NOT IN', 'AND'))) {
                $tax_operator = 'IN';
            }

            $tax_args = array(
                'tax_query' => array(
                    array(
                        'taxonomy'         => $taxonomy,
                        'field'            => 'slug',
                        'terms'            => $tax_term,
                        'operator'         => $tax_operator,
                        'include_children' => $tax_include_children,
                    ),
                ),
            );

            // Check for multiple taxonomy queries
            $count            = 2;
            $more_tax_queries = false;
            while (
                isset($original_atts['taxonomy_' . $count]) && !empty($original_atts['taxonomy_' . $count]) &&
                isset($original_atts['tax_' . $count . '_term']) && !empty($original_atts['tax_' . $count . '_term'])
            ):

                // Sanitize values
                $more_tax_queries     = true;
                $taxonomy             = sanitize_key($original_atts['taxonomy_' . $count]);
                $terms                = explode(', ', sanitize_text_field($original_atts['tax_' . $count . '_term']));
                $tax_operator         = isset($original_atts['tax_' . $count . '_operator']) ? $original_atts['tax_' . $count . '_operator'] : 'IN';
                $tax_operator         = in_array($tax_operator, array('IN', 'NOT IN', 'AND')) ? $tax_operator : 'IN';
                $tax_include_children = isset($original_atts['tax_' . $count . '_include_children']) ? filter_var($atts['tax_' . $count . '_include_children'], FILTER_VALIDATE_BOOLEAN) : true;

                $tax_args['tax_query'][] = array(
                    'taxonomy'         => $taxonomy,
                    'field'            => 'slug',
                    'terms'            => $terms,
                    'operator'         => $tax_operator,
                    'include_children' => $tax_include_children,
                );

                $count++;

            endwhile;

            if ($more_tax_queries):
                $tax_relation = 'AND';
                if (isset($original_atts['tax_relation']) && in_array($original_atts['tax_relation'], array('AND', 'OR'))) {
                    $tax_relation = $original_atts['tax_relation'];
                }

                $args['tax_query']['relation'] = $tax_relation;
            endif;

            $args = array_merge($args, $tax_args);
        }

        // If post parent attribute, set up parent
        if ($post_parent !== false) {
            if ('current' == $post_parent) {
                global $post;
                $post_parent = get_the_ID();
            }
            $args['post_parent'] = intval($post_parent);
        }
        return apply_filters('display_posts_shortcode_args', $args, $original_atts);
    }
}
