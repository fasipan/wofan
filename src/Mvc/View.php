<?php
namespace WFan\Mvc;

/**
 *
 */
use Timber\Site;
use Timber\Timber;
use Timber\User;
use WFan\Traits\PathTrait;

class View extends Timber
{
    public $menu;

    public $engine;

    use PathTrait;

    public function __construct($views = [])
    {
        $default = [];
        if ($views == []) {
            $default = is_array(self::$dirname) ? self::$dirname : array(self::$dirname);
        }
        self::$locations = array_merge($views, $default);
        self::$dirname   = array_merge($views, $default);
        add_filter('timber_context', array($this, 'addContext'));
        add_filter('get_twig', array($this, 'addTwig'));
        add_action('init', array($this, 'addThemeSupports'));
        parent::__construct();
    }

    public function register_post_types()
    {
        //this is where you can register custom post types
    }

    public function register_taxonomies()
    {
        //this is where you can register custom taxonomies
    }

    public function getEngine()
    {
        return $this->engine;
    }

    public function addContext($context)
    {
        $context['site']        = new Site();
        $context['user']        = new User();
        $context['options']     = self::getOptions();
        $context['paginations'] = self::getPagination();
        $configBreadcrumb       = \Config::get("components.breadcrumb");
        $configBreadcrumb       = $configBreadcrumb ?: [];
        $context['breadcrumb']  = (new \Breadcrumb($configBreadcrumb))->get_output();
        return $context;
    }

    public static function getOptions()
    {
        global $site_options;
        if ($site_options) {
            return array_merge($site_options, wp_load_alloptions());
        }
        return wp_load_alloptions();
    }

    public static function getPagination()
    {
        $paginations = self::get_pagination();
        ob_start();
        self::render("components/paginations/paginations.twig", array(
            "paginations" => $paginations,
        ));
        $paginations = ob_get_contents();
        ob_clean();
        return $paginations;
    }

    public function addTwig($twig)
    {
        /* this is where you can add your own fuctions to twig */
        $twig->addExtension(new \Twig_Extension_StringLoader());
        $twig->addExtension(new \Twig_Extensions_Extension_Text());
        $twig->addExtension(new \Twig_Extensions_Extension_I18n());
        $twig->addExtension(new \Twig_Extensions_Extension_Array());
        $twig->addExtension(new \Twig_Extensions_Extension_Date());

        $twig->addFilter(new \Twig_SimpleFilter("do_scode", function ($string) {
            return do_shortcode($string);
        }));
        $twig->addFilter(new \Twig_SimpleFilter("url", function ($string) {
            return do_shortcode($string);
        }));

        $twig->addFilter(new \Twig_SimpleFilter("perlink", function ($id) {
            return get_permalink($id);
        }));

        $filters = array_merge(\Config::get('filters-core'), \Config::get('filters'));
        $filter  = new \Filter($twig, $filters);
        return $filter->make()->getEngine();
    }

    public function addThemeSupports()
    {
        global $site_options;
        global $admin_options;
    }

    public static function make($template, $context = [])
    {
        $templates = (is_array($template)) ? $template : array($template);

        $templates = array_map(function ($value) {
            return $value . ".twig";
        }, $templates);

        $content = self::fetch($templates, array_merge(self::get_context(), $context));
        return do_shortcode($content);
    }

    public static function show($template, $context = [])
    {
        echo self::make($template, $context);
    }

}
