<?php
namespace WFan\Mvc;

use Themosis\Facades\PostType as BasePostType;

/**
 * Model
 */
class Model extends BasePostType
{
    public static $type = "post";

    public static $label = "";

    public static $labels = "";

    public static $supports = "";

    public $post;

    public function __construct($object = null)
    {
        if ($object) {
            $this->post = $object;
        }
    }

    public static function afterCreate()
    {

    }

    public static function create()
    {

        if (static::$type) {
            static::$label = static::$label ?: ucwords(static::$type);
            $labels        = array(
                'name'               => _x(static::$label, 'post type general name'),
                'singular_name'      => _x(static::$label, 'post type singular name'),
                'add_new'            => _x('Add New ' . static::$label, 'new '),
                'add_new_item'       => __("Add New " . static::$label),
                'edit_item'          => __("Edit " . static::$label),
                'new_item'           => __("New " . static::$label),
                'view_item'          => __("View " . static::$label),
                'search_items'       => __("Search " . static::$label),
                'not_found'          => __('No ' . static::$type . ' found'),
                'not_found_in_trash' => __('No ' . static::$type . ' found in Trash'),
                'parent_item_colon'  => '',

            );
            parent::make(static::$type, 'Tours', static::$label)->set([
                'public'        => true,
                'label'         => static::$label . "s",
                'labels'        => $labels,
                'menu_position' => 20,
                'supports'      => ['title', 'editor', 'thumbnail', 'excerpt'],
                'rewrite'       => true,
                'query_var'     => false,
            ]);

            self::afterCreate();
        }

    }

    public function addTaxonomy()
    {

    }

    public static function all()
    {

    }

    public static function find($id = null)
    {
        if ($id) {
            return \View::get_post($args);
        }
        return self::query();
    }

    public static function finds($args = null)
    {
        return self::query($args);
    }

    public function setPost($post)
    {

    }

    public static function query($params = array())
    {
        $args = array_merge(array(
            'posts_per_page'   => 5,
            'offset'           => 0,
            'category'         => '',
            'category_name'    => '',
            'orderby'          => 'date',
            'order'            => 'DESC',
            'include'          => '',
            'exclude'          => '',
            'meta_key'         => '',
            'meta_value'       => '',
            'post_type'        => static::$type,
            'post_mime_type'   => '',
            'post_parent'      => '',
            'author'           => '',
            'author_name'      => '',
            'post_status'      => 'publish',
            'suppress_filters' => true,
        ), $params);

        return \View::get_posts($args);
    }

    public function term($name)
    {
        $terms = get_the_terms($this->id, $name);
        $data  = [];
        if ($terms) {
            foreach ($terms as $key => $term) {
                $term->link = esc_url(get_term_link($term, $name));
                $data[]     = $term;
            }
        }
        return $data;
    }

}
