<?php
namespace WFan\Mvc;

use Illuminate\Routing\Controller as CoreController;
use WFan\Traits\TemplateTrait;

/**
 * Controller
 */

class Controller extends CoreController
{
    use TemplateTrait;

    public $templates = [];

    public $context = [];

    public $section = null;

    public $view = null;

    public $breadcrumbs = null;

    public $paginations = null;

    public function __construct()
    {
        $this->setup();
        $this->onConstruct();
    }

    public function onConstruct()
    {

    }

    public function mapTemplateToContent()
    {
        global $wp_query;
        $templates = array();
        $post;
        $posts;

    }

    public function setup()
    {
        global $wp_query;

        $templates = array();
        $post      = f_get_post();
        $posts     = \View::get_posts();

        $this->context["post"]  = $post;
        $this->context["posts"] = $posts;

        if (is_rtl()) {
            $templates[] = 'rtl';
        } elseif (is_front_page() && is_home()) {
            // Default homepage
            $this->context['page'] = $post;
            $templates[]           = 'page-home';
        } elseif (is_front_page()) {
            // static homepage
            $this->context['page'] = $post;
            $templates[]           = 'page-home';

        } elseif (is_home()) {
            $this->context['page']  = $post;
            $this->context['posts'] = $posts;
            $templates[]            = 'page-blog';

        } elseif (is_singular()) {

            if (is_single()) {
                $model = null;
                if (isset($post->post_type)) {

                    $post_type = sanitize_html_class($post->post_type, $post->id);
                    $post_id   = $post->id;

                    $templates[] = $post_type . "-" . $post_id;
                    $templates[] = $post_type . "-" . sanitize_html_class($post->slug, $post_id);
                    $templates[] = $post_type;

                    $post->TermClass = "WFan\Mvc\Models\Term";
                    $postClass       = "\Models\\" . studly_case($post_type);

                    if (class_exists($postClass)) {
                        $model = new $postClass($post);
                    }
                    // Post Format
                    if (post_type_supports($post->post_type, 'post-formats')) {
                        $post_format = get_post_format($post->id);
                        if ($post_format && !is_wp_error($post_format)) {
                            $templates[] = 'single-format-' . sanitize_html_class($post_format);
                        } else {
                            $templates[] = 'single-format-standard';
                        }
                    }
                }
                if (is_attachment()) {
                    $mime_type   = get_post_mime_type($post_id);
                    $mime_prefix = array('application/', 'image/', 'text/', 'audio/', 'video/', 'music/');
                    $templates[] = 'attachment-' . $post_id;
                    $templates[] = 'attachment-' . str_replace($mime_prefix, '', $mime_type);
                }
                $templates[]                     = 'single';
                $this->context['post']           = $post;
                $this->context[$post->post_type] = ($model) ? $model : $post;
                if ($post->post_type == "product") {
                    $this->context[$post->post_type] = f_get_product($post);
                }

            } elseif (is_page()) {

                $page_id = $wp_query->get_queried_object_id();

                $templates[] = 'page-' . $page_id;
                $templates[] = 'page-' . $post->post_name;
                if (get_pages(array('parent' => $page_id, 'number' => 1))) {
                    $templates[] = 'page-parent';
                }
                if ($post->post_parent) {
                    $templates[] = 'page-child';
                    $templates[] = 'parent-pageid-' . $post->post_parent;
                }
                if (is_page_template()) {
                    $template_slug  = get_page_template_slug($page_id);
                    $template_parts = explode('/', $template_slug);

                    foreach ($template_parts as $part) {
                        $templates[] = 'page-template-' . sanitize_html_class(str_replace(array('.', '/'), '-', basename($part, '.php')));
                    }
                    $templates[] = 'page-template-' . sanitize_html_class(str_replace('.', '-', $template_slug));
                    $templates[] = 'page-template';
                } else {
                    $templates[] = 'page-template-default';
                }
                $templates[] = 'page';

                $this->context['post'] = $this->context['page'] = f_get_post();

            }
            $templates[] = 'singular';

        } elseif (is_archive()) {

            $term       = new \WFan\Mvc\Models\Term();
            $term_class = sanitize_html_class($term->slug, $term->id);
            $term_slug  = str_replace("_", "-", $term->taxonomy);

            if (is_post_type_archive()) {
                $post_type = get_query_var('post_type');

                if (is_array($post_type)) {
                    $post_type = reset($post_type);
                }

                // is_shop
                if ($post_type == "product") {
                    $templates[]               = "page-shop";
                    $this->context['products'] = f_get_products();
                }

                $templates[] = 'page-' . sanitize_html_class($post_type);
                $templates[] = 'post-type-archive-' . sanitize_html_class($post_type);
                $templates[] = 'post-type-archive';

            } elseif (is_author()) {

                $author = $term;

                if (isset($author->user_nicename)) {
                    $templates[] = 'author-' . $author->ID;
                    $templates[] = 'author-' . sanitize_html_class($author->user_nicename, $author->ID);
                }

                $templates[] = 'author';

            } elseif (is_category()) {

                $cat = $term;

                if (isset($cat->id)) {

                    $cat_class = $term_class;
                    if (is_numeric($cat_class) || !trim($cat_class, '-')) {
                        $cat_class = $cat->id;
                    }
                    $templates[] = 'category-' . $cat->id;
                    $templates[] = 'category-' . $cat_class;

                }
                $templates[] = 'category';

            } elseif (is_tag()) {

                $tag = $term;
                if (isset($tag->id)) {

                    $tag_class = $term_class;
                    if (is_numeric($tag_class) || !trim($tag_class, '-')) {
                        $tag_class = $tag->id;
                    }

                    $templates[] = 'tag-' . $tag->id;
                    $templates[] = 'tag-' . $tag_class;
                }
                $templates[] = 'tag';

            } elseif (is_tax()) {

                if (isset($term->id)) {

                    $templates[] = $term_slug . "-" . $term->id;
                    $templates[] = $term_slug . "-" . $term->slug;
                    $templates[] = $term_slug;

                    if (is_numeric($term_class) || !trim($term_class, '-')) {
                        $term_class = $term->id;
                    }
                    $templates[] = 'tax-' . sanitize_html_class($term_slug);
                    $templates[] = 'term-' . $term->id;
                    $templates[] = 'term-' . $term_class;

                    $termClass = "\WebQuick\Models\\" . studly_case($term_slug);
                    if (class_exists($termClass)) {
                        // do something object
                    }
                }
            }

            $post_type = explode("-", $term->taxonomy)[0];
            $args      = array(
                'post_type'     => $post_type,
                $term->taxonomy => $term->slug,
            );
            $this->context[$post_type . "s"] = \View::get_posts($args);
            $this->context['posts']          = \View::get_posts($args);
            $this->context["post"]           = $term;
            $this->context["term"]           = $term;
            $this->context[$term_slug]       = $term;

            // is_catalogue
            if ($term_slug == "product-cat") {
                $this->context["catalogue"] = $term;
                $this->context['products']  = f_get_products(array(
                    "catalogue" => $term->slug,
                ));
                $templates[] = "catalogue";
            }

            $templates[] = 'archive';

        } elseif (is_date()) {
            if (is_day()) {
                $this->context['title'] = 'Archive: ' . get_the_date('D M Y');
            } elseif (is_month()) {
                $this->context['title'] = 'Archive: ' . get_the_date('M Y');
            } elseif (is_year()) {
                $this->context['title'] = 'Archive: ' . get_the_date('Y');
            }
        } elseif (is_404()) {
            $templates[] = '404';

        } elseif (is_search()) {
            $this->context['title'] = 'Search results for ' . get_search_query();
            $this->context['posts'] = $posts;
            $templates[]            = 'search';

        } elseif (is_comments_popup()) {

        }
        $templates[] = 'index';

        $this->templates = array_map(function ($value) {
            return "templates/" . $value;
        }, $templates);

    }

}
