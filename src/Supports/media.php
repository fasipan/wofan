<?php
if (!function_exists('f_get_term_thumbnail_id')) {
    function f_get_term_thumbnail_id($term_id)
    {
        $term_id = absint($term_id);

        if ($term_id) {
            $thumbnail_id = get_term_meta($term_id, '_thumbnail_id', true);
            return $thumbnail_id ? absint($thumbnail_id) : false;
        }

        return false;
    }
}

if (!function_exists('f_has_term_thumbnail')) {
    function f_has_term_thumbnail($term_id)
    {
        return (bool) f_get_term_thumbnail_id($term_id);
    }
}

if (!function_exists('f_get_term_thumbnail')) {
    function f_get_term_thumbnail($term_id, $size = 'post-thumbnail', $attr = '')
    {
        $id_thumbnail = f_get_term_thumbnail_id($term_id);
        return wp_get_attachment_url($id_thumbnail, $size, $attr);
    }
}
