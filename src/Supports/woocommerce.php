<?php
function is_woocommerce_activated()
{
    return class_exists('woocommerce') ? true : false;
}

if (is_woocommerce_activated()) {

    function f_get_products($attrs = array())
    {
        $limit = (isset($attrs["limit"])) ? $attrs["limit"] : 5;
        $args  = array(
            'post_type'           => 'product',
            'post_status'         => 'publish',
            'ignore_sticky_posts' => 1,
            'posts_per_page'      => $limit,
        );

        if (isset($attrs["catalogue"])) {
            $args['product_cat'] = $attrs["catalogue"];
        }

        if (isset($attrs["featured"])) {
            $args['meta_query'] = array(
                array(
                    'key'     => '_featured',
                    'value'   => 'yes',
                    'compare' => '=',
                ),
            );
        }

        if (isset($attrs["top_sale"])) {
            $args['meta_query'] = array(
                array(
                    'key'     => 'total_sales',
                    'value'   => 0,
                    'compare' => '>',
                ),
            );
        }
        $posts = Timber\Timber::get_posts($args, "Timber\Post");
        return array_map(function ($post) {
            return new WFan\Mvc\Models\Product($post);
        }, $posts);
    }

    if (!function_exists('f_get_product')) {
        function f_get_product($product)
        {
            if (is_object($product)) {
                return new WFan\Mvc\Models\Product($product);
            }

            $product = get_product($product);
            if ($product) {
                return new WFan\Mvc\Models\Product($product);
            }

        }
    }

    if (!function_exists('f_get_catalogue')) {
        function f_get_catalogue()
        {
            $queried_object       = get_queried_object();
            $term_id              = $queried_object->term_id;
            $catalogue            = get_term($term_id, 'product_cat');
            $thumbnail_id         = get_woocommerce_term_meta($term_id, 'thumbnail_id', true);
            $catalogue->thumbnail = wp_get_attachment_url($thumbnail_id);
            return $catalogue;
        }
    }

}

function __woocommerce_show_product_thumbnails()
{
    echo "</br>product_thumbnails";
}

function __woocommerce_pagination()
{
    echo "</br>pagination";
}
function __woocommerce_catalog_ordering()
{
    echo "</br>ordering";
}

function __woocommerce_get_sidebar()
{
    echo "</br>woocommerce_get_sidebar";
}

function __woocommerce_get_product_thumbnail()
{
    echo "</br>woocommerce_get_product_thumbnail";
}

function __woocommerce_template_single_rating()
{
    echo "</br>woocommerce_template_single_rating";
}

function __woocommerce_show_product_images()
{
    echo "</br>woocommerce_show_product_images";
    woocommerce_get_product_thumbnail();
}

function __woocommerce_output_product_data_tabs()
{
    echo "</br>woocommerce_output_product_data_tabs";
}

function __woocommerce_template_single_price()
{
    echo "</br>woocommerce_template_single_price";
}

function __woocommerce_template_single_excerpt()
{
    echo "</br>woocommerce_template_single_excerpt";
}

function __woocommerce_template_single_meta()
{
    echo "</br>woocommerce_template_single_meta";
}

function __woocommerce_template_single_sharing()
{
    echo "</br>woocommerce_template_single_sharing";
}

function __woocommerce_show_product_sale_flash()
{
    echo "</br>woocommerce_show_product_sale_flash";
}

function __woocommerce_template_single_add_to_cart()
{
    echo "</br>woocommerce_template_single_add_to_cart";
}

function __woocommerce_simple_add_to_cart()
{
    echo "</br>woocommerce_simple_add_to_cart";
}

function __woocommerce_grouped_add_to_cart()
{
    echo "</br>woocommerce_grouped_add_to_cart";
}

function __woocommerce_external_add_to_cart()
{
    echo "</br>woocommerce_external_add_to_cart";
}

function __woocommerce_quantity_input()
{
    echo "</br>woocommerce_quantity_input";
}

function __woocommerce_product_description_tab()
{
    echo "</br>woocommerce_product_description_tab";
}

function __woocommerce_product_additional_information_tab()
{
    echo "</br>woocommerce_product_additional_information_tab";
}

function __woocommerce_product_reviews_tab()
{
    echo "</br>woocommerce_product_reviews_tab";
}

function __woocommerce_default_product_tabs()
{
    echo "</br>woocommerce_default_product_tabs";
}

function __woocommerce_sort_product_tabs()
{
    echo "</br>woocommerce_sort_product_tabs";
}

function __woocommerce_comments()
{
    echo "</br>woocommerce_comments";
}

function __woocommerce_review_display_gravatar()
{
    echo "</br>woocommerce_review_display_gravatar";
}

function __woocommerce_review_display_rating()
{
    echo "</br>woocommerce_review_display_rating";
}

function __woocommerce_review_display_meta()
{
    echo "</br>woocommerce_review_display_meta";
}

function __woocommerce_review_display_comment_text()
{
    echo "</br>woocommerce_review_display_comment_text";
}

function __woocommerce_output_related_products()
{
    echo "</br>woocommerce_output_related_products";
}

function __woocommerce_related_products()
{
    echo "</br>woocommerce_related_products";
}

function __woocommerce_mini_cart()
{
    echo "</br>woocommerce_mini_cart";
}

function __woocommerce_breadcrumb()
{
    echo "</br>woocommerce_breadcrumb";
}

function __woocommerce_template_single_title()
{
    echo "</br>woocommerce_template_single_title";
}
