<?php

if (!function_exists('lavel_setup')) {
    function lavel_setup()
    {
        load_theme_textdomain('lavel', get_template_directory() . '/languages');
        $locale      = get_locale();
        $locale_file = get_template_directory() . "/languages/{$locale}.php";
        if (is_readable($locale_file)) {require_once $locale_file;}

        global $content_width;
        if (!isset($content_width)) {
            $content_width = 720;
        }
        add_editor_style(array('assets/css/editor-style.css'));
    }
    add_action('init', 'lavel_setup');
}

if (!function_exists('dd')) {
    /**
     * Dump the passed variables and end the script.
     *
     * @param  mixed
     * @return void
     */
    function dd()
    {
        if (!empty(func_get_args())) {
            echo "<pre>";
            call_user_func_array("var_dump", func_get_args());
        }
        die(1);
    }
}

if (!function_exists('f_get_post')) {
    function f_get_post()
    {
        $post = new Timber\Post();

        if (!isset($post->panels_data)) {
            $post->f_post_content = $post->post_content;
        } else {
            $content              = get_the_content();
            $content              = apply_filters('the_content', $content);
            $content              = str_replace(']]>', ']]&gt;', $content);
            $post->f_post_content = $content;
        }
        $post->f_post_content = $post->f_post_content;
        return $post;
    }
}

function timber_set_product($post)
{
    global $product;
    if (class_exists('WooCommerce') && is_woocommerce()) {
        $product = get_product($post->ID);
    }
}

function fan_attr($context, $attributes = array())
{

    $attributes = fan_parse_attr($context, $attributes);

    $output = '';

    //* Cycle through attributes, build tag attribute string
    foreach ($attributes as $key => $value) {

        if (!$value) {
            continue;
        }

        if (true === $value) {
            $output .= esc_html($key) . ' ';
        } else {
            $output .= sprintf('%s="%s" ', esc_html($key), esc_attr($value));
        }

    }

    $output = apply_filters("fan_attr_{$context}_output", $output, $attributes, $context);

    return trim($output);

}
function fan_parse_attr($context, $attributes = array())
{

    $defaults = array(
        'class' => sanitize_html_class($context),
    );

    $attributes = wp_parse_args($attributes, $defaults);

    //* Contextual filter
    return apply_filters("fan_attr_{$context}", $attributes, $context);

}
function fan_html5()
{
    return current_theme_supports('html5');
}
