<?php
namespace WFan\Configs;

use WFan\Traits\IncludeTrait;

/**
 * Plugin Sevice
 */
class Plugin
{
    use IncludeTrait;
    public $_plugins = array();

    public function __construct($plugins)
    {
        $this->_plugins = $plugins;
    }

    public function make()
    {
        $this->_require();
    }

    protected function _require()
    {
        add_action('tgmpa_register', function () {
            $configs = array(
                'menu'         => 'tp_plugin_install',
                'has_notice'   => true,
                'dismissable'  => false,
                'is_automatic' => true,
            );
            tgmpa($this->_getPlugins(), $configs);
        });
    }

    protected function _getPlugins()
    {
        $plugins = array();
        global $admin_options;

        foreach ($this->_plugins as $key => $plugin) {
            $isAdmin = (isset($plugin["isAdmin"])) ? $plugin["isAdmin"] : false;
            if ($admin_options[$plugin["slug"] . "_active"] || $isAdmin) {
                $plugins[] = $plugin;
            }
        }
        return $plugins;
    }

    public function getOptionAdmin()
    {
        $options = array();
        foreach ($this->_plugins as $key => $plugin) {
            $options[] = array(
                'id'       => $plugin["slug"] . "_active",
                'type'     => 'switch',
                'title'    => __('Actice ' . $plugin["name"], 'nubade'),
                'subtitle' => __('Actice ' . $plugin["name"]),
                'desc'     => __('Actice ' . $plugin["name"]),
                'default'  => '0',
            );
        }
        return $options;
    }
}
