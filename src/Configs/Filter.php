<?php
namespace WFan\Configs;

use WFan\Traits\IncludeTrait;

class Filter
{
    use IncludeTrait;

    protected $_filters = [];
    protected $_engine;

    public function __construct($twig, $filters)
    {
        $this->_filters = $filters;
        $this->_engine  = $twig;
    }

    public function make()
    {
        if (count($this->_filters) > 0) {
            foreach ($this->_filters as $key => $filter) {
                if (is_callable($filter["callable"])) {
                    $this->_engine->addFilter(new \Twig_SimpleFilter($filter["name"], $filter["callable"]));
                }
            }
        }
        return $this;
    }

    public function getEngine()
    {
        return $this->_engine;
    }
}
