<?php
namespace WFan\Configs;

use WFan\Traits\IncludeTrait;

class Shortcode
{
    use IncludeTrait;
    protected $_shortcodes = [];
    public function __construct($shortcodes)
    {
        $this->_shortcodes = $shortcodes;

    }
    public function make()
    {
        if (count($this->_shortcodes) > 0) {
            foreach ($this->_shortcodes as $key => $shortcode) {
                if (is_callable($shortcode["callable"])) {
                    add_shortcode($shortcode['name'], $shortcode['callable']);
                }
            }
        }
    }
}
