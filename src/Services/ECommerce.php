<?php
namespace WFan\Services;

use WFan\Traits\IncludeTrait;

/**
 * ECommerce
 */

class ECommerce
{
    use IncludeTrait;

    public $isSale = false;

    public $isCustomizeStyle = false;

    public $active = false;

    public function __construct()
    {
        global $site_options;
        global $admin_options;
        $this->isSale           = (isset($site_options["ecommerce_sale_active"])) ? $site_options["ecommerce_sale_active"] : false;
        $this->isCustomizeStyle = (isset($site_options["ecommerce_style_active"])) ? $site_options["ecommerce_style_active"] : false;
        $this->isCustomizeView  = (isset($site_options["ecommerce_view_active"])) ? $site_options["ecommerce_view_active"] : false;
        $this->active           = (isset($admin_options["woocommerce_active"])) ? $admin_options["woocommerce_active"] : false;
        $this->addStyle();
        $this->activeSale();
    }
    /**
     * [activeSale description]
     * @return [type] [description]
     */
    public function activeSale()
    {
        if ($this->isSale) {

        }
    }

    public function addStyle()
    {
        if ($this->active && $this->isCustomizeStyle) {

            add_filter('woocommerce_enqueue_styles', '__return_false');

            add_action("wp_enqueue_scripts", function () {

                // $path_1 = 'assets/css/woocommerce-ux.css';
                // if (file_exists($this->getDirTheme($path_1))) {
                //     wp_enqueue_style('woocm-uxqode', $this->getPathTheme($path_1));
                // } else {
                //     wp_enqueue_style('woocm-uxqode', $this->getPathFramework("frontend/" . $path_1));
                // }

                // $path_2 = 'assets/css/woo-layout-ux.css';
                // if (file_exists($this->getDirTheme($path_2))) {
                //     wp_enqueue_style('woo-layout-ux', $this->getPathTheme($path_2));
                // } else {
                //     wp_enqueue_style('woo-layout-ux', $this->getPathFramework("frontend/" . $path_2));
                // }
            });

        }

    }

    public function getOptionSite()
    {
        return $this->getIncludes("options.site.ecommerce");
    }

    public function overrideView()
    {
        if ($this->isCustomizeView) {

        }
    }
}
