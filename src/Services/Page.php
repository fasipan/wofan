<?php
namespace WFan\Services;

use Timber\Timber;

/**
 * Page
 */
class Page
{

    protected $_current;
    public function __construct()
    {
        $this->initialize();
    }
    public function getCurrent()
    {
        return $this->_current;
    }

    public function initialize()
    {
        $page = [];
        if (class_exists('WooCommerce') && is_woocommerce()) {
            if (is_shop()) {
                $post              = f_get_post();
                $page["page_name"] = "shop";
                $page["title"]     = $post->post_title;

            } elseif (is_product()) {
                $product           = Timber::get_post();
                $page["page_name"] = "product";
                $page["title"]     = $product->post_title;
                $page["content"]   = $product;

            } elseif (is_product_category()) {
                $posts             = Timber::get_posts();
                $queried_object    = get_queried_object();
                $term_id           = $queried_object->term_id;
                $post              = get_term($queried_object->term_id, 'product_cat');
                $page["page_name"] = "catalogue";
                $page["title"]     = $post->name;
                $page["content"]   = $post;

            } elseif (is_product_tag()) {
                $page["page_name"] = "product_tag";
            } elseif (is_account_page()) {
                $page["page_name"] = "user";
            } elseif (is_cart()) {
                $page["page_name"] = "cart";
            } elseif (is_add_payment_method_page()) {
                $page["page_name"] = "payment";
            } elseif (is_ajax()) {

            } elseif (is_checkout()) {
                $page["page_name"] = "checkout";
            } elseif (is_checkout_pay_page()) {
                $page["page_name"] = "checkout";
            } elseif (is_filtered()) {

            } elseif (is_lost_password_page()) {

            } elseif (is_order_received_page()) {

            } elseif (is_product_taxonomy()) {

            } elseif (is_store_notice_showing()) {

            } elseif (is_view_order_page()) {

            }
        } elseif (is_singular()) {
            if (is_single()) {
                $post              = Timber::query_post();
                $page["content"]   = $post;
                $page["page_name"] = "post";
                $page["title"]     = $post->title;
                if (is_attachment()) {
                    $page["page_name"] = "attachment";
                } elseif (post_password_required($post->ID)) {

                } else {

                }

            } elseif (is_page()) {
                $post              = f_get_post();
                $page["page_name"] = "page";
                $page["title"]     = $post->post_title;
                $page["content"]   = $post;
            }
        } elseif (is_archive()) {
            if (is_author()) {
                if ($author_id = get_query_var('author')) {
                    $author = get_user_by('id', $author_id);
                }

                if (isset($wp_query->query_vars['author'])) {
                    $author = new TimberUser($wp_query->query_vars['author']);
                }
                $page              = $author;
                $page["page_name"] = "author";
            } elseif (is_category()) {
                $post              = get_category(get_query_var('cat'));
                $page["page_name"] = "category";
                $page["title"]     = $post->name;
                $page["content"]   = $post;

            } elseif (is_tag()) {
                $post              = get_queried_object();
                $page['title']     = single_tag_title('', false);
                $page["page_name"] = "tag";
                $page["title"]     = "tag";
                $page["post"]      = $post;
            } elseif (is_tax()) {

            } elseif (is_date()) {
                if (is_day()) {
                    $page["page_name"] = "day";
                } elseif (is_month()) {
                    $page["page_name"] = "month";
                } elseif (is_year()) {
                    $page["page_name"] = "year";
                }
            } elseif (is_post_type_archive()) {
                $page['title']     = post_type_archive_title('', false);
                $page["page_name"] = "archive";

            }
        } elseif (is_front_page() && is_home()) {
            // Default homepage
            $page            = f_get_post();
            $page->page_name = "home";
        } elseif (is_front_page()) {
            // static homepage
            $page            = f_get_post();
            $page->page_name = "home";

        } elseif (is_home()) {
            $page["page_name"] = "blog";
        } elseif (is_404()) {
            $page["page_name"] = "404";
        } elseif (is_search()) {
            $page['title']     = 'Search results for ' . get_search_query();
            $page["page_name"] = "search";
        } elseif (is_comments_popup()) {

        }
        $this->_current = $page;
    }
}
