<?php
namespace WebQuick\Services;

use WebQuick\Traits\IncludeTrait;

class Bundle
{
    use IncludeTrait;
    public $_scripts = array();
    public $_styles  = array();

    public function __construct($styles = array(), $scripts = array())
    {
        $scripts        = $this->getInclude("scripts");
        $styles         = $this->getInclude("styles");
        $this->_scripts = array_merge($scripts, $this->_scripts);
        $this->_styles  = array_merge($styles, $this->_styles);
        if (!is_admin()) {
            add_action('wp_enqueue_scripts', array($this, "addJs"));
            add_action('wp_enqueue_scripts', array($this, "addCss"));
            // add_action('wp_enqueue_style', array($this, "addCss"));
            // $this->addJs();
            // $this->addCss();
        }
    }

    public function addJs()
    {
        foreach ($this->_scripts as $key => $script) {
            call_user_func_array('wp_enqueue_script', $script);
            // wp_enqueue_script($handle, $source, $dependencies, $version, $in_footer);
        }
    }
    public function addCss()
    {
        foreach ($this->_styles as $key => $style) {
            call_user_func_array('wp_enqueue_style', $style);
            // wp_enqueue_style($handle, $source, $dependencies, $version, $in_footer);
        }
    }
}
