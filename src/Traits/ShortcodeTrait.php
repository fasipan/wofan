<?php

namespace WFan\Traits;

use Phalcon\Text;

trait ShortcodeTrait
{

    protected $_tags = [];

    public function addShortcode($tag, $func = null)
    {
        $this->_tags[$tag] = $func;

        $this->getDI()->getShortcodes()->add($this->getBaseName() . ":" . $tag, $this);

        return $this;
    }

    public function doShortcode($attr, $content, $tag)
    {
        // use Phalcon\Cache\Backend\Memcache;
        // use Phalcon\Cache\Frontend\Data;
        // $prefix               = $this->getBaseName() . ":" . $tag;
        // $key                  = md5(serialize([$attr, $content]));
        // $config               = $this->configs->get("cache.memcache")->toArray();
        // $config["prefix"]     = $prefix;
        // $config["persistent"] = true;

        // $cache = new Memcache(
        //     new Data([
        //         "lifetime" => 3600,
        //     ]),
        //     $config
        // );

        // $content = $cache->get($key);

        // if ($content === null) {
        //     $func   = isset($this->_tags[$tag]) ? $this->_tags[$tag] : false;
        //     $method = Text::camelize("do_shortcode_" . $tag);

        //     if ($func) {
        //         $content = $func($attr, $content, $tag);
        //     } else if (method_exists($this, $method)) {
        //         $content = $this->$method($attr, $content, $tag);
        //     } else {
        //         return "Shortcode [$method] handle not found on " . $this->getBaseName();
        //     }

        //     $content = $this->shortcodes->doShortcodes($content);

        //     $cache->save($key, $content);
        // }

        // return $content;

        $func   = isset($this->_tags[$tag]) ? $this->_tags[$tag] : false;
        $method = Text::camelize("do_shortcode_" . $tag);

        if ($func) {
            $content = $func($attr, $content, $tag);
        } else if (method_exists($this, $method)) {
            $content = $this->$method($attr, $content, $tag);
        } else {
            return "Shortcode [$method] handle not found on " . $this->getBaseName();
        }

        $content = $this->shortcodes->doShortcodes($content);

        return $content;

    }

}
