<?php

namespace WFan\Traits;

trait TemplateTrait
{
    protected $_engine;

    public function render($template = array(), $context = array())
    {

        echo $this->html($template, $context);

    }

    public function html($template = array(), $context = array())
    {
        $app  = $GLOBALS['application'];
        $view = $app->container['fview'];

        $this->_engine  = $view;
        $templateObject = array();
        $contextObject  = array();
        $templates      = array();
        $contexts       = array();
        if (isset($this->templates) && $this->templates) {
            $templateObject = $this->templates;
        }

        $template  = (is_array($template)) ? $template : array($template);
        $templates = array_merge($template, $templateObject);

        if (isset($this->context) && $this->context) {
            $contextObject = $this->context;
        }

        $contexts = array_merge($context, $contextObject);
        return $this->_engine->make($templates, $contexts);
    }
    public function __html($template = array(), $context = array())
    {
        $app  = $GLOBALS['application'];
        $view = $app->container['fview'];

        $this->_engine  = $view;
        $templateObject = array();
        $contextObject  = array();
        $templates      = array();
        $contexts       = array();
        if (isset($this->templates) && $this->templates) {
            $templateObject = $this->templates;
        }

        $template = (is_array($template)) ? $template : array($template);

        $templates = array_merge($template, $templateObject);

        $templates = array_map(function ($value) {
            return $value . ".twig";
        }, $templates);

        if (isset($this->context) && $this->context) {
            $contextObject = $this->context;
        }
        $contexts = array_merge($context, $contextObject, $this->_engine->get_context());
        ob_start();
        $this->_engine->render($templates, $contexts);
        return do_shortcode(ob_get_clean());
    }

}
