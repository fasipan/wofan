<?php
namespace WFan\Traits;

trait PathTrait
{

    protected $_pathFramework = "/vendor/fasipan/nudepress/src/";

    protected $_pathFrameworkView = "/vendor/fasipan/nudepress/public/views/";

    protected $_pathThemeView = "views/";

    public function getPathTheme($path)
    {
        return get_template_directory_uri() . "/" . $path;
    }

    public function getPathFramework($path)
    {
        return get_template_directory_uri() . $this->_pathFramework . $path;
    }

    public function getDirTheme($path)
    {
        return get_template_directory() . "/" . $path;
    }

    public function getDirFramework($path)
    {
        return get_template_directory() . $this->_pathFramework . $path;
    }

}
