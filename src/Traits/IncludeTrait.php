<?php
namespace WFan\Traits;

trait IncludeTrait
{
    use PathTrait;

    protected $_includesDir = "includes/";

    protected $_includesCoreDir = "frontend/includes/";

    protected $_includeExtension = "php";

    protected $_included = [];

    public function isIncludable($name)
    {
        $path     = $this->getDirTheme($this->_includesDir . "$name." . $this->_includeExtension);
        $pathCore = $this->getDirFramework($this->_includesCoreDir . "$name." . $this->_includeExtension);
        if (file_exists($path) && is_file($path)) {
            return $path;
        } elseif (file_exists($pathCore)) {
            return $pathCore;
        }
        return false;
    }

    public function getInclude($name)
    {
        $path = $this->isIncludable($name);
        if ($path) {
            if (!isset($this->_included[$path])) {
                $this->_included[$path] = include $path;
            }
            return $this->_included[$path];
        }
        return false;
    }

    public function getIncludes($name)
    {
        if (strpos($name, '.') !== false) {
            $name = str_replace('.', '/', $name);
        }
        $path = $this->getDirTheme($this->_includesDir . "$name." . $this->_includeExtension);

        $pathCore      = $this->getDirFramework($this->_includesCoreDir . "$name." . $this->_includeExtension);
        $includesCore  = [];
        $includesTheme = [];
        if (file_exists($path) && is_file($path)) {
            $includesTheme = include $path;
        }
        if (file_exists($pathCore) && is_file($pathCore)) {
            $includesCore = include $pathCore;
        }
        $includes = array_merge($includesCore, $includesTheme);
        return array_values($includes);
    }
}
