<?php
namespace WFan\Options;

/**
 * OptionAdmin
 */
class Admin extends AbstractOption
{

    public function onConstruct()
    {
        $this->setArguments();
        $this->setHelpTabs();
        $this->setSections();
    }

    public function setHelpTabs()
    {

        // Custom page help tabs, displayed using the help API. Tabs are shown in order of definition.
        $this->args['help_tabs'][] = array(
            'id'      => 'redux-help-tab-1',
            'title'   => __('Theme Information 1', 'nubade'),
            'content' => __('<p>This is the tab content, HTML is allowed.</p>', 'nubade'),
        );

        $this->args['help_tabs'][] = array(
            'id'      => 'redux-help-tab-2',
            'title'   => __('Theme Information 2', 'nubade'),
            'content' => __('<p>This is the tab content, HTML is allowed.</p>', 'nubade'),
        );

        // Set the help sidebar
        $this->args['help_sidebar'] = __('<p>This is the sidebar content, HTML is allowed.</p>', 'nubade');
    }

    public function setArguments()
    {

        $theme = wp_get_theme(); // For use with some settings. Not necessary.

        $this->args = array(
            'opt_name'           => 'admin_options',
            'display_name'       => 'Admin Options',
            'display_version'    => $theme->get('Version'),
            'menu_type'          => 'menu',
            'allow_sub_menu'     => true,
            'menu_title'         => __('Admin Options', 'nubade'),
            'page_title'         => __('Admin Options', 'nubade'),
            'google_api_key'     => '',

            'async_typography'   => false,
            'admin_bar'          => true,
            'global_variable'    => '',
            'dev_mode'           => false,
            'customizer'         => true,

            'page_priority'      => null, // Order where the menu appears in the admin area. If there is any conflict, something will not show. Warning.
            'page_parent'        => 'themes.php', // For a full list of options, visit: http://codex.wordpress.org/Function_Reference/add_submenu_page#Parameters
            'page_permissions'   => 'manage_options', // Permissions needed to access the options panel.
            'menu_icon'          => '', // Specify a custom URL to an icon
            'last_tab'           => '', // Force your panel to always open to a specific tab (by id)
            'page_icon'          => 'icon-themes', // Icon displayed in the admin panel next to your menu_title
            'page_slug'          => '_admin_options', // Page slug used to denote the panel
            'save_defaults'      => true, // On load save the defaults to DB before user clicks save or not
            'default_show'       => false, // If true, shows the default value next to each field that is not the default value.
            'default_mark'       => '', // What to print by the field's title if the value shown is default. Suggested: *
            'show_import_export' => true, // Shows the Import/Export panel when not used as a field.

            // CAREFUL -> These options are for advanced use only
            'transient_time'     => 60 * MINUTE_IN_SECONDS,
            'output'             => true, // Global shut-off for dynamic CSS output by the framework. Will also disable google fonts output
            'output_tag'         => true, // Allows dynamic CSS to be generated for customizer and google fonts, but stops the dynamic CSS from going to the head
            'footer_credit'      => 'NuBade Theme Optional Panel', // Disable the footer credit of Redux. Please leave if you can help it.

            // FUTURE -> Not in use yet, but reserved or partially implemented. Use at your own risk.
            'database'           => '', // possible: options, theme_mods, theme_mods_expanded, transient. Not fully functional, warning!
            'system_info'        => false, // REMOVE

            // HINTS
            'hints'              => array(
                'icon'          => 'icon-question-sign',
                'icon_position' => 'right',
                'icon_color'    => 'lightgray',
                'icon_size'     => 'normal',
                'tip_style'     => array(
                    'color'   => 'light',
                    'shadow'  => true,
                    'rounded' => false,
                    'style'   => '',
                ),
                'tip_position'  => array(
                    'my' => 'top left',
                    'at' => 'bottom right',
                ),
                'tip_effect'    => array(
                    'show' => array(
                        'effect'   => 'slide',
                        'duration' => '500',
                        'event'    => 'mouseover',
                    ),
                    'hide' => array(
                        'effect'   => 'slide',
                        'duration' => '500',
                        'event'    => 'click mouseleave',
                    ),
                ),
            ),
        );

        // SOCIAL ICONS -> Setup custom links in the footer for quick links in your panel footer icons.
        $this->args['share_icons'][] = array(
            'url'   => 'https://github.com/fasipan',
            'title' => 'Visit us on GitHub',
            'icon'  => 'el-icon-github',
            //'img'   => '', // You can use icon OR img. IMG needs to be a full URL.
        );
        $this->args['share_icons'][] = array(
            'url'   => 'https://www.facebook.com/fasipan',
            'title' => 'Like us on Facebook',
            'icon'  => 'el-icon-facebook',
        );
        $this->args['share_icons'][] = array(
            'url'   => 'http://twitter.com/fasipan',
            'title' => 'Follow us on Twitter',
            'icon'  => 'el-icon-twitter',
        );
        $this->args['share_icons'][] = array(
            'url'   => 'http://www.linkedin.com/company/fasipan',
            'title' => 'Find us on LinkedIn',
            'icon'  => 'el-icon-linkedin',
        );

        // Add content after the form.
        $this->args['footer_text'] = __('<p> Admin Option NuBade Theme Create By <a href="http://fasipan.com/">Fasipan</a>.</p>', 'nubade');
    }

    public function setSections()
    {

        /**
        Used within different fields. Simply examples. Search for ACTUAL DECLARATION for field examples
         * */
        // Background Patterns Reader
        $sample_patterns_path = \ReduxFramework::$_dir . '../sample/patterns/';
        $sample_patterns_url  = \ReduxFramework::$_url . '../sample/patterns/';
        $sample_patterns      = array();

        if (is_dir($sample_patterns_path)):

            if ($sample_patterns_dir = opendir($sample_patterns_path)):
                $sample_patterns = array();

                while (($sample_patterns_file = readdir($sample_patterns_dir)) !== false) {

                    if (stristr($sample_patterns_file, '.png') !== false || stristr($sample_patterns_file, '.jpg') !== false) {
                        $name              = explode('.', $sample_patterns_file);
                        $name              = str_replace('.' . end($name), '', $sample_patterns_file);
                        $sample_patterns[] = array('alt' => $name, 'img' => $sample_patterns_url . $sample_patterns_file);
                    }
                }
            endif;
        endif;

        $ct            = wp_get_theme();
        $this->theme   = $ct;
        $item_name     = $this->theme->get('Name');
        $tags          = $this->theme->Tags;
        $plugins       = array_merge(\Config::get('plugins-core'), \Config::get('plugins'));
        $pluginService = new \Plugin($plugins);
        // ACTUAL DECLARATION OF SECTIONS
        $this->sections[] = array(
            'title'  => __('Home Settings', 'nubade'),
            'icon'   => 'el-icon-home',
            'fields' => array(
                array(
                    'id'       => 'admin_style_active',
                    'type'     => 'switch',
                    'title'    => __('Actice Style Admin', 'nubade'),
                    'subtitle' => __('Actice Style Admin  '),
                    'default'  => '1',
                ),
            ),
        );
        $options_content_type = apply_filters('add_admin_options_content_type', array());
        $this->sections[]     = array(
            'icon'   => 'el-icon-eye-open',
            'title'  => __('Content Type', 'nubade'),
            'desc'   => __('<p class="description">You can change theme fonts.</p>', 'nubade'),
            'fields' => array(),
        );
        foreach ($options_content_type as $key => $option_content_type) {
            $this->sections[] = $option_content_type;
        }
        $this->sections[] = array(
            'title'  => __('Plugin Actice Settings', 'nubade'),
            'icon'   => 'el-icon-home',
            'fields' => $pluginService->getOptionAdmin(),
        );

    }
}
