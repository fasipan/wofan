<?php
namespace WFan\Options;

use WFan\Traits\IncludeTrait;

/**
 * AbstractOption
 */

if (!class_exists('AdminOption')) {
    class AbstractOption
    {
        use IncludeTrait;

        public $args     = array();
        public $sections = array();
        public $theme;
        public $ReduxFramework;
        public $options;

        public function __construct()
        {

            if (!class_exists('ReduxFramework')) {
                return;
            }
            // This is needed. Bah WordPress bugs.  ;)
            if (true == \Redux_Helpers::isTheme(__FILE__)) {
                $this->onConstruct();
            } else {
                add_action('plugins_loaded', array($this, 'onConstruct'), 10);
            }
            /**
             * hide note redux (hack)
             */
            add_action('init', function () {
                if (class_exists('ReduxFrameworkPlugin')) {
                    remove_filter('plugin_row_meta', array(\ReduxFrameworkPlugin::get_instance(), 'plugin_metalinks'), null, 2);
                }
                if (class_exists('ReduxFrameworkPlugin')) {
                    remove_action('admin_notices', array(\ReduxFrameworkPlugin::get_instance(), 'admin_notices'));
                }
            });

            if (!isset($this->args['opt_name'])) {
                // No errors please
                return;
            }
            $this->ReduxFramework = new \ReduxFramework($this->sections, $this->args);
        }

        public function onConstruct()
        {

        }

        public function setHelpTabs()
        {

        }

        public function setArguments()
        {

        }

        public function setSections()
        {

        }

        /**

        This is a test function that will let you see when the compiler hook occurs.
        It only runs if a field    set with compiler=>true is changed.

         * */
        public function compiler_action($options, $css, $changed_values)
        {
            echo '<h1>The compiler hook has run!</h1>';
            echo "<pre>";
            print_r($changed_values); // Values that have changed since the last save
            echo "</pre>";
        }

        /**

        Custom function for filtering the sections array. Good for child themes to override or add to the sections.
        Simply include this function in the child themes functions.php file.

        NOTE: the defined constants for URLs, and directories will NOT be available at this point in a child theme,
        so you must use get_template_directory_uri() if you want to use any of the built in icons

         * */
        public function dynamic_section($sections)
        {
            //$sections = array();
            $sections[] = array(
                'title'  => __('Section via hook', 'nubade'),
                'desc'   => __('<p class="description">This is a section created by adding a filter to the sections array. Can be used by child themes to add/remove sections from the options.</p>', 'nubade'),
                'icon'   => 'el-icon-paper-clip',
                // Leave this as a blank section, no options just some intro text set above.
                'fields' => array(),
            );

            return $sections;
        }

        /**

        Filter hook for filtering the args. Good for child themes to override or add to the args array. Can also be used in other functions.

         * */
        public function change_arguments($args)
        {
            //$args['dev_mode'] = true;

            return $args;
        }

        /**

        Filter hook for filtering the default value of any given field. Very useful in development mode.

         * */
        public function change_defaults($defaults)
        {
            $defaults['str_replace'] = 'Testing filter hook!';

            return $defaults;
        }

        // Remove the demo link and the notice of integrated demo from the redux-framework plugin
        public function remove_demo()
        {

            // Used to hide the demo mode link from the plugin page. Only used when Redux is a plugin.
            if (class_exists('ReduxFrameworkPlugin')) {
                remove_filter('plugin_row_meta', array(\ReduxFrameworkPlugin::instance(), 'plugin_metalinks'), null, 2);

                // Used to hide the activation notice informing users of the demo panel. Only used when Redux is a plugin.
                remove_action('admin_notices', array(\ReduxFrameworkPlugin::instance(), 'admin_notices'));
            }
        }
    }
}
