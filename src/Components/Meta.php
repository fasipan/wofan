<?php
namespace WFan\Components;

use Themosis\Metabox\Meta as BaseMeta;

/**
 *
 */
class Meta extends BaseMeta
{

    public static function term($id, $key = '', $single = true)
    {
        $default = get_term_meta($id, $key, $single);
        if (is_ssl()) {
            return static::parse($default);
        }

        return $default;
    }
}
