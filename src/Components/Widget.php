<?php
namespace WFan\Components;

use WFan\Traits\PathTrait;

/**
 * Widget
 */
class Widget
{
    use PathTrait;
    const PATH_WIDGET = "includes/Widgets/";

    public function __construct()
    {
        $this->setup();
    }

    public function setup()
    {

        $pathCore  = $this->getDirFramework('frontend/' . self::PATH_WIDGET);
        $pathTheme = $this->getDirTheme(self::PATH_WIDGET);
        foreach (glob($pathCore . '*.php') as $path) {
            $filename = basename($path);
            require $pathCore . '/' . $filename;
            $class = basename($path, ".php");
            add_action('widgets_init', function () use ($class) {
                register_widget("$class");
            });
        }
        foreach (glob($pathTheme . '*.php') as $path) {
            $filename = basename($path);
            require $pathTheme . '/' . $filename;
            $class = basename($path, ".php");
            add_action('widgets_init', function () use ($class) {
                register_widget("$class");
            });
        }
    }
}
