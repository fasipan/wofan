<?php
namespace WFan\Components;

use Themosis\Facades\PostType as BasePostType;

/**
 * PostType
 */
class PostType extends BasePostType
{
    public $type = "post";

    public function __construct()
    {

    }

    public function posts($params = array())
    {
        $args = array_merge(array(
            'posts_per_page'   => 5,
            'offset'           => 0,
            'category'         => '',
            'category_name'    => '',
            'orderby'          => 'date',
            'order'            => 'DESC',
            'include'          => '',
            'exclude'          => '',
            'meta_key'         => '',
            'meta_value'       => '',
            'post_type'        => $this->type,
            'post_mime_type'   => '',
            'post_parent'      => '',
            'author'           => '',
            'author_name'      => '',
            'post_status'      => 'publish',
            'suppress_filters' => true,
        ), $params);

        return \View::get_posts($args);
    }

    public function post()
    {

    }
}
