<?php
namespace WFan\Components;

use Themosis\Config\Sidebar as BaseSidebar;
use Timber\Timber;
use WFan\Traits\IncludeTrait;
use WFan\Traits\TemplateTrait;

class Sidebar extends BaseSidebar
{
    use IncludeTrait, TemplateTrait;

    public function __construct($data = array())
    {
        parent::__construct($data);
        /**
         * add shortcode sidebar
         */
        add_shortcode("sections", function ($attrs) {
            $name = sanitize_text_field($attrs['name']);

            shortcode_atts(array(
                "name"    => "sidebar",
                "display" => $name,
            ), $attrs, 'sections');

            $display  = isset($attrs["display"]) ? $attrs["display"] : str_replace("_", "-", $name);
            $displays = array($display, "master");
            return $this->get($name, $displays);
        });
    }

    public function get($name, $display)
    {
        $display = (is_array($display)) ? $display : array($display);

        $ct = Timber::get_context();

        $ct["widgets"] = Timber::get_widgets($name);
        $display       = array_map(function ($item) {
            return 'views/sections/' . $item;
        }, $display);

        return $this->html($display, $ct);
    }
}
