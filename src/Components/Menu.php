<?php
namespace WFan\Components;

use Themosis\Config\Menu as BaseMenu;
use WFan\Traits\IncludeTrait;
use WFan\Traits\TemplateTrait;

/**
 * Menu
 */
class Menu extends BaseMenu
{
    use IncludeTrait, TemplateTrait;

    public function __construct($data = [])
    {
        parent::__construct($data);
    }

    public function get($name, $display)
    {
        $display = is_array($display) ? $display : array($display);
        $display = array_map(function ($item) {
            return "components/menus/" . $item;
        }, $display);
        return $this->html(
            $display,
            array(
                "menu" => new \Timber\Menu($name),
            )
        );
    }

}
