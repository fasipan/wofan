<?php
namespace WFan\Components;

/**
 * Pagination
 */
class Pagination
{

    public function __construct()
    {
    }

    public static function get()
    {
        $paginations = \Timber\Timber::get_pagination();
        ob_start();
        \Timber\Timber::render("components/paginations/paginations.twig", array(
            "paginations" => $paginations,
        ));
        $paginations = ob_get_contents();
        ob_clean();
        return $paginations;
    }
}
