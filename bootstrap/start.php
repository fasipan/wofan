<?php

(new Template(Config::get('templates'), new FilterBuilder($app->container)))->make();

(new Support(Config::get('supports')))->make();

(new Sidebar(Config::get('sidebars')))->make();

(new Menu(Config::get('menus')))->make();

(new Images(Config::get('images'), new FilterBuilder($app->container)))->make();

(new Constant(Config::get('constants')))->make();

$plugins = array_merge(Config::get('plugins-core'), Config::get('plugins'));
(new Plugin($plugins))->make();

$shortcodes = array_merge(Config::get('shortcodes-core'), Config::get('shortcodes'));
(new Shortcode($shortcodes))->make();

/**
 * load router
 */
require_once PATH_FRAMEWORK . DS . "public" . DS . 'routes.php';

require_once global_path('apps') . 'routes.php';

new WFan\Options\Admin();
new WFan\Options\Site();
