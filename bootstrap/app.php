<?php
if (!class_exists('WFan')) {
    class WFan
    {
        /**
         * Themosis instance.
         *
         * @var \Themosis
         */
        protected static $instance = null;

        /**
         * Framework version.
         *
         * @var float
         */
        const VERSION = '1.2.3';

        /**
         * The service container.
         *
         * @var \Themosis\Foundation\Application
         */
        public $container;

        private function __construct()
        {
            $this->autoload();
            $this->bootstrap();
        }

        /**
         * Retrieve Themosis class instance.
         *
         * @return \Themosis
         */
        public static function instance()
        {
            if (is_null(static::$instance)) {
                static::$instance = new static();
            }

            return static::$instance;
        }

        /**
         * Check for the composer autoload file.
         */
        protected function autoload()
        {
            // Check if there is a autoload.php file.
            // Meaning we're in development mode or
            // the plugin has been installed on a "classic" WordPress configuration.
            if (file_exists($autoload = __DIR__ . DS . 'vendor' . DS . 'autoload.php')) {
                require $autoload;

                // Developers using the framework in a "classic" WordPress
                // installation can activate this by defining
                // a WFAN_ERROR constant and set its value to true or false
                // depending of their environment.
                if (defined('WFAN_ERROR') && WFAN_ERROR) {
                    $whoops = new \Whoops\Run();
                    $whoops->pushHandler(new \Whoops\Handler\PrettyPageHandler());
                    $whoops->register();
                }
            }
        }

        /**
         * Bootstrap the core plugin.
         */
        protected function bootstrap()
        {
            /*
             * Define core framework paths.
             * These are real paths, not URLs to the framework files.
             */
            $paths['core'] = PATH_FRAMEWORK . DS;

            $paths['sys'] = PATH_FRAMEWORK . DS . 'src' . DS . 'Themosis';

            $paths['views_core'] = PATH_FRAMEWORK . DS . 'public' . DS . 'views';

            $paths['assets_core'] = PATH_FRAMEWORK . DS . 'public' . DS . 'assets';

            $paths['configs_core'] = PATH_FRAMEWORK . DS . 'public' . DS . 'configs';

            $paths['storage'] = WFAN_STORAGE;

            global_set_paths($paths);

            /*
             * Instantiate the service container for the project.
             */
            $this->container = new \Themosis\Foundation\Application();

            /*
             * Create a new Request instance and register it.
             * By providing an instance, the instance is shared.
             */
            $request = \Themosis\Foundation\Request::capture();
            $this->container->instance('request', $request);

            /*
             * Setup the facade.
             */
            \Themosis\Facades\Facade::setFacadeApplication($this->container);

            /*
             * Register into the container, the registered paths.
             * Normally at this stage, plugins should have
             * their paths registered into the $GLOBALS array.
             */
            $this->container->registerAllPaths(global_path());

            /*
             * Register core service providers.
             */
            $this->registerProviders();

            /*
             * Setup core.
             */
            $this->setup();

            /*
             * Project hooks.
             * Added in their called order.
             */
            add_action('admin_enqueue_scripts', [$this, 'adminEnqueueScripts']);
            add_action('admin_head', [$this, 'adminHead']);
            add_action('template_redirect', 'redirect_canonical');
            add_action('template_redirect', 'wp_redirect_admin_locations');
            add_action('template_redirect', [$this, 'setRouter'], 20);
        }

        /**
         * Register core framework service providers.
         */
        protected function registerProviders()
        {
            /*
             * Service providers.
             */
            $providers = apply_filters('themosis_service_providers', [
                Themosis\Ajax\AjaxServiceProvider::class,
                Themosis\Asset\AssetServiceProvider::class,
                Themosis\Config\ConfigServiceProvider::class,
                Themosis\Database\DatabaseServiceProvider::class,
                Themosis\Field\FieldServiceProvider::class,
                Themosis\Finder\FinderServiceProvider::class,
                Themosis\Hook\HookServiceProvider::class,
                Themosis\Html\FormServiceProvider::class,
                Themosis\Html\HtmlServiceProvider::class,
                Themosis\Load\LoaderServiceProvider::class,
                Themosis\Metabox\MetaboxServiceProvider::class,
                Themosis\Page\PageServiceProvider::class,
                Themosis\Page\Sections\SectionServiceProvider::class,
                Themosis\PostType\PostTypeServiceProvider::class,
                Themosis\Route\RouteServiceProvider::class,
                Themosis\Taxonomy\TaxonomyServiceProvider::class,
                Themosis\User\UserServiceProvider::class,
                Themosis\Validation\ValidationServiceProvider::class,
                Themosis\View\ViewServiceProvider::class,
            ]);

            foreach ($providers as $provider) {
                $this->container->register($provider);
            }
        }

        /**
         * Setup core framework parameters.
         * At this moment, all activated plugins have been loaded.
         * Each plugin has its service providers registered.
         */
        protected function setup()
        {
            /*
             * Add view paths.
             */
            $viewFinder = $this->container['view.finder'];
            $viewFinder->addLocation(global_path('views_core') . 'metabox' . DS);
            $viewFinder->addLocation(global_path('views_core') . 'page' . DS);
            $viewFinder->addLocation(global_path('views_core') . 'posttype' . DS);
            $viewFinder->addLocation(global_path('views_core') . 'field' . DS);
            $viewFinder->addLocation(global_path('views_core') . 'taxonomy' . DS);
            $viewFinder->addLocation(global_path('views_core') . 'user' . DS);

            /*
             * Add paths to asset finder.
             */
            $url         = get_template_directory_uri() . DS . "vendor/fasipan/wofan/public/assets/";
            $assetFinder = $this->container['asset.finder'];
            $assetFinder->addPaths([$url => global_path('assets_core')]);

            /*
             * Add framework core assets URL to the global
             * admin JS object.
             */
            add_filter('themosisAdminGlobalObject', function ($data) use ($url) {
                $data['_themosisAssets'] = $url;

                return $data;
            });

            /*
             * Register framework media image size.
             */
            $images = new Themosis\Config\Images([
                '_themosis_media' => [100, 100, true, __('Mini', FAN_TEXTDOMAIN)],
            ], $this->container['filter']);
            $images->make();

            /*
             * Register framework assets.
             */
            $this->container['asset']->add('themosis-core-styles', 'css/_themosisCore.css', ['wp-color-picker'])->to('admin');
            $this->container['asset']->add('themosis-core-scripts', 'js/_themosisCore.js', ['jquery', 'jquery-ui-sortable', 'underscore', 'backbone', 'mce-view', 'wp-color-picker'], '1.3.0', true)->to('admin');
        }

        /**
         * Hook into front-end routing.
         * Setup the router API to be executed before
         * theme default templates.
         */
        public function setRouter()
        {
            if (is_feed() || is_comment_feed()) {
                return;
            }

            $request  = $this->container['request'];
            $response = $this->container['router']->dispatch($request);
            // We only send back the content because, headers are already defined
            // by WordPress internals.
            $response->sendContent();
        }

        /**
         * Enqueue Admin scripts.
         */
        public function adminEnqueueScripts()
        {
            /*
             * Make sure the media scripts are always enqueued.
             */
            wp_enqueue_media();
        }

        /**
         * Output a global JS object in the <head> tag for the admin.
         * Allow developers to add JS data for their project in the admin area only.
         */
        public function adminHead()
        {
            $datas = apply_filters('themosisAdminGlobalObject', []);

            $output = "<script type=\"text/javascript\">\n\r";
            $output .= "//<![CDATA[\n\r";
            $output .= "var themosisAdmin = {\n\r";

            if (!empty($datas)) {
                foreach ($datas as $key => $value) {
                    $output .= $key . ': ' . json_encode($value) . ",\n\r";
                }
            }

            $output .= "};\n\r";
            $output .= "//]]>\n\r";
            $output .= '</script>';

            // Output the datas.
            echo $output;
        }
    }
}
