<?php

$app                    = WFan::instance();
$GLOBALS['application'] = $app;

$configFinder = $app->container['config.finder'];

$configFinder->addPaths([
    "configs_core" => PATH_FRAMEWORK . DS . "public/configs" . DS,
    "configs"      => PATH_THEME . DS . "configs" . DS,
]);

/**
 * [config paths ]
 *
 */
$pathsCore = Themosis\Facades\Config::get('paths-core');
$pathsCore = array_map(function ($item) {
    return PATH_FRAMEWORK . $item;
}, $pathsCore);

$pathsTheme = Themosis\Facades\Config::get('paths');
$pathsTheme = array_map(function ($item) {
    return PATH_THEME . DS . $item . DS;
}, $pathsTheme);

$paths = array_merge($pathsCore, $pathsTheme);
global_set_paths($paths);
/**
 * [load alias ]
 *
 */
$aliasesCore  = Themosis\Facades\Config::get('app-core.aliases');
$aliasesTheme = Themosis\Facades\Config::get('app.aliases');
$aliases      = array_merge($aliasesCore, $aliasesTheme);
if (!empty($aliases) && is_array($aliases)) {
    foreach ($aliases as $alias => $fullname) {
        @class_alias($fullname, $alias);
    }
}

$assetFinder = $app->container['asset.finder'];
$viewFinder  = $app->container['view.finder'];
$assetFinder->addPaths([
    get_template_directory_uri() . "/assets" => global_path("assets"),
]);

$viewFinder->addLocation([
    "views" => global_path("views"),
]);

$views = array_merge([global_path("views")], [global_path("views_core")]);

$app->container->singleton('fview', function () use ($views) {
    return new View($views);
});

/**
 * load widgets
 */
(new WidgetLoader(new FilterBuilder($app->container)))
    ->add([
        global_path('apps') . DS . 'Widgets',
    ])
    ->load();
(new WidgetLoader(new FilterBuilder($app->container)))
    ->add([
        global_path('apps-core') . DS . 'Widgets',
    ])
    ->load();
/**
 * load Labraries
 */
foreach (glob(PATH_FRAMEWORK . '/src/Labraries/*.php') as $path) {
    $filename = basename($path);
    require_once PATH_FRAMEWORK . '/src/Labraries/' . $filename;
}

/**
 * load Supports
 */
foreach (glob(PATH_FRAMEWORK . '/src/Supports/*.php') as $path) {
    $filename = basename($path);
    require_once PATH_FRAMEWORK . '/src/Supports/' . $filename;
}

require_once __DIR__ . DS . 'start.php';
