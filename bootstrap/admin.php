<?php
/**
 * custom editor
 */
add_filter('tiny_mce_before_init', function ($init) {
    $init['theme_advanced_blockformats'] = 'p,h2,h3,h4,h5,h6,pre,i,span';
    $init['verify_html']                 = false;
    return $init;
});
