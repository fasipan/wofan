<?php
return array(
    "site" => array(
        "info"   => array(
            array(
                'id'       => 'site_logo_text',
                'type'     => 'text',
                'title'    => __('Site Logo Text', FAN_TEXTDOMAIN),
                'compiler' => 'true',
                'mode'     => false,
                'subtitle' => __('Site Logo Text for your website', FAN_TEXTDOMAIN),
            ),
            array(
                'id'       => 'site_email',
                'type'     => 'text',
                'title'    => __('Site Email', FAN_TEXTDOMAIN),
                'compiler' => 'true',
                'mode'     => false,
                'subtitle' => __('Site Email for your website', FAN_TEXTDOMAIN),
            ),
            array(
                'id'       => 'site_phone',
                'type'     => 'text',
                'title'    => __('Site Phone', FAN_TEXTDOMAIN),
                'compiler' => 'true',
                'mode'     => false,
                'subtitle' => __('Site Phone for your website', FAN_TEXTDOMAIN),
            ),
            array(
                'id'       => 'site_address',
                'type'     => 'text',
                'title'    => __('Site Address', FAN_TEXTDOMAIN),
                'compiler' => 'true',
                'mode'     => false,
                'subtitle' => __('Site Address for your website', FAN_TEXTDOMAIN),
            ),
            array(
                'id'       => 'site_description',
                'type'     => 'editor',
                'title'    => __('Site Description', FAN_TEXTDOMAIN),
                'compiler' => 'true',
                'mode'     => false,
                'subtitle' => __('Site Description for your website', FAN_TEXTDOMAIN),
            ),
        ),
        "photo"  => array(
            array(
                'id'       => 'logo',
                'type'     => 'media',
                'title'    => __('Logo Normal', FAN_TEXTDOMAIN),
                'compiler' => 'true',
                'mode'     => false, // Can be set to false to allow any media type, or can also be set to any mime type.
                'subtitle' => __('Upload header logo for your website', FAN_TEXTDOMAIN),
            ),
            array(
                'id'       => 'logo-footer',
                'type'     => 'media',
                'title'    => __('Logo Footer', FAN_TEXTDOMAIN),
                'compiler' => 'true',
                'mode'     => false, // Can be set to false to allow any media type, or can also be set to any mime type.
                'subtitle' => __('Upload Footer logo for your website', FAN_TEXTDOMAIN),
            ),
            array(
                'id'       => 'favicon',
                'type'     => 'media',
                'title'    => __('Favicon', FAN_TEXTDOMAIN),
                'compiler' => 'true',
                'mode'     => false, // Can be set to false to allow any media type, or can also be set to any mime type.
                'subtitle' => __('Upload favicon for your website', FAN_TEXTDOMAIN),
            ),
            array(
                'id'       => 'preloader-logo',
                'type'     => 'media',
                'title'    => __('Logo in the Preloader', FAN_TEXTDOMAIN),
                'compiler' => 'true',
                'mode'     => false, // Can be set to false to allow any media type, or can also be set to any mime type.
                'subtitle' => __('Upload logo to be displayed on prelaoder', FAN_TEXTDOMAIN),
            ),
        ),
        "social" => array(
            array(
                'id'       => 'social_facebook',
                'type'     => 'text',
                'title'    => __('Facebook URL', 'nubade'),
                'validate' => 'url',
            ),
            array(
                'id'       => 'social_twitter',
                'type'     => 'text',
                'title'    => __('Twitter URL', 'nubade'),
                'validate' => 'url',
            ),
            array(
                'id'       => 'social_googlep',
                'type'     => 'text',
                'title'    => __('Google Plus URL', 'nubade'),
                'validate' => 'url',
            ),
            array(
                'id'       => 'social_linkedin',
                'type'     => 'text',
                'title'    => __('LinkedIn URL', 'nubade'),
                'validate' => 'url',
            ),
            array(
                'id'       => 'social_pinterest',
                'type'     => 'text',
                'title'    => __('Pinterest URL', 'nubade'),
                'validate' => 'url',
            ),
            array(
                'id'       => 'social_skype',
                'type'     => 'text',
                'title'    => __('Skype URL', 'nubade'),
                'validate' => 'url',
            ),
            array(
                'id'       => 'social_dribbble',
                'type'     => 'text',
                'title'    => __('Dribbble URL', 'nubade'),
                'validate' => 'url',
            ),
            array(
                'id'       => 'social_tumblr',
                'type'     => 'text',
                'title'    => __('Tumblr URL', 'nubade'),
                'validate' => 'url',
            ),
            array(
                'id'       => 'social_youtube',
                'type'     => 'text',
                'title'    => __('Youtube URL', 'nubade'),
                'validate' => 'url',
            ),
            array(
                'id'       => 'social_vimeo',
                'type'     => 'text',
                'title'    => __('Vimeo URL', 'nubade'),
                'validate' => 'url',
            ),
        ),
    ),
);
