<?php
return [
    [
        "name"     => "d_scode",
        "callable" => function ($string) {
            return do_shortcode($string);
        },
    ],
    [
        "name"     => "file",
        "callable" => function ($id) {
            return wp_get_attachment_url($id);
        },
    ],
];
