 <?php
return array(
    "img"          => array(
        "name"     => "img",
        "callable" => function ($attrs) {
            echo "<img src=" . $attrs["src"] . "/>";
        },
    ),
    "options"      => array(
        "name"     => "options",
        "callable" => function ($attrs) {
            $options = View::getOptions();
            if (isset($options[$attrs["name"]])) {
                return $options[$attrs["name"]];
            }
            return "not_found_options_[" . $attrs["name"] . "]";
        },
    ),
    "menus"        => array(
        "name"     => "menus",
        "callable" => function ($attrs) {
            $menu     = new Menu();
            $name     = isset($attrs["name"]) ? $attrs["name"] : "primary";
            $display  = (isset($attrs["display"])) ? $attrs["display"] : $name;
            $displays = array($display, $name, "master");
            return $menu->get($name, $displays);
        },
    ),
    "e-products"   => array(
        "name"     => "e-products",
        "callable" => function ($attrs) {
            $name    = isset($attrs['name']) ? sanitize_text_field($attrs['name']) : "master";
            $name    = $name ?: "master";
            $display = sanitize_text_field($attrs['display']);
            $display = $display ? $display . "s" : "lists";
            $col     = (isset($attrs["col"])) ? $attrs["col"] : 3;

            $products = f_get_products($attrs);

            $displays = ["components/$display/product-$name.twig", "components/$display/product.twig"];
            return View::fetch($displays, ["products" => $products, "col" => $col]);
        },
    ),
    "e-catalogues" => array(
        "name"     => "e-catalogues",
        "callable" => function ($attrs) {

            $name    = isset($attrs['name']) ? sanitize_text_field($attrs['name']) : "master";
            $name    = $name ?: "master";
            $display = sanitize_text_field($attrs['display']);
            $display = $display ? $display . "s" : "lists";
            $col     = (isset($attrs["col"])) ? $attrs["col"] : 3;

            $catalogues = \Timber\TermGetter::get_terms('product_cat', array(), "\WFan\Mvc\Models\Term");

            $catalogues = array_map(function ($item) {
                $id_thumbnail    = get_woocommerce_term_meta($item->term_id, 'thumbnail_id', true);
                $item->thumbnail = new \Timber\Image($id_thumbnail);
                return $item;
            }, $catalogues);
            $displays = ["components/$display/catalogue-$name.twig", "components/$display/catalogue.twig"];
            return View::fetch($displays, ["catalogues" => $catalogues, "col" => $col]);
        },
    ),
    "posts"        => array(
        "name"     => "posts",
        "callable" => function ($attrs) {
            $post = new WFan\Mvc\Models\Post();

            $name    = isset($attrs['name']) ? sanitize_text_field($attrs['name']) : "master";
            $name    = $name ?: "master";
            $display = sanitize_text_field($attrs['display']);
            $display = $display ? $display . "s" : "lists";

            $attrs = shortcode_atts($post->attrs, $attrs, 'posts');
            $posts = $post->getPosts($post->getParams($attrs));

            $displays = ["components/$display/post-$name.twig", "components/$display/post.twig"];
            return View::fetch($displays, ["posts" => $posts]);
        },
    ),
);
