<?php

return [

    /* --------------------------------------------------------------- */
    // Theme class aliases
    /* --------------------------------------------------------------- */
    'aliases' => [
        'Action'        => 'Themosis\\Facades\\Action',
        'Ajax'          => 'Themosis\\Facades\\Ajax',
        'Asset'         => 'Themosis\\Facades\\Asset',
        'Config'        => 'Themosis\\Facades\\Config',
        'Controller'    => 'Illuminate\Routing\Controller',
        'Field'         => 'Themosis\\Facades\\Field',
        'Form'          => 'Themosis\\Facades\\Form',
        'Html'          => 'Themosis\\Facades\\Html',
        'Input'         => 'Themosis\\Facades\\Input',
        'Meta'          => 'WFan\\Components\\Meta',
        'Metabox'       => 'Themosis\\Facades\\Metabox',
        'Option'        => 'Themosis\\Page\\Option',
        'Page'          => 'Themosis\\Facades\\Page',
        'PostType'      => 'WFan\\Components\\PostType',
        'Route'         => 'Themosis\\Facades\\Route',
        'Section'       => 'Themosis\\Facades\\Section',
        'TaxField'      => 'Themosis\\Taxonomy\\TaxField',
        'TaxMeta'       => 'Themosis\\Taxonomy\\TaxMeta',
        'Taxonomy'      => 'Themosis\\Facades\\Taxonomy',
        'User'          => 'Themosis\\Facades\\User',
        'Validator'     => 'Themosis\\Facades\\Validator',
        'Loop'          => 'Themosis\\Facades\\Loop',
        // 'View'         => 'Themosis\\Facades\\View',
        'Template'      => 'Themosis\\Config\\Template',
        'FilterBuilder' => '\Themosis\Hook\FilterBuilder',
        'Support'       => 'Themosis\\Config\\Support',
        'Sidebar'       => 'WFan\\Components\\Sidebar',
        'Menu'          => 'WFan\\Components\\Menu',
        'Images'        => 'Themosis\\Config\\Images',
        'Constant'      => 'Themosis\\Config\\Constant',
        'WidgetLoader'  => 'Themosis\Load\WidgetLoader',
        'View'          => 'WFan\Mvc\View',
        'Plugin'        => 'WFan\Configs\Plugin',
        'Shortcode'     => 'WFan\Configs\Shortcode',
        'Filter'        => 'WFan\Configs\Filter',
        'Breadcrumb'    => 'WFan\Components\Breadcrumb',

    ],

];
