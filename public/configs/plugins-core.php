<?php
return array(
    "redux-framework"   => array(
        'name'    => 'Redux Framework',
        'slug'    => 'redux-framework',
        "isAdmin" => true,
    ),
    "siteorigin-panels" => array(
        'name'    => 'Page Builder by SiteOrigin',
        'slug'    => 'siteorigin-panels',
        "isAdmin" => false,
    ),
    "woocommerce"       => array(
        'name'    => 'Woocommerce',
        'slug'    => 'woocommerce',
        "desc"    => "Ecommerce",
        "isAdmin" => false,
    ),
);
