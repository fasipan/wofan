<?php

/*----------------------------------------------------*/
// The directory separator.
/*----------------------------------------------------*/

defined('DS') ? DS : define('DS', DIRECTORY_SEPARATOR);

defined('PATH_THEME') ? PATH_THEME : define('PATH_THEME', get_template_directory());

defined('URI_THEME') ? URI_THEME : define('URI_THEME', get_template_directory_uri());

defined('PATH_FRAMEWORK') ? PATH_FRAMEWORK : define('PATH_FRAMEWORK', __DIR__);

defined('URI_FRAMEWORK') ? URI_FRAMEWORK : define('URI_FRAMEWORK', get_template_directory_uri() . DS . "vendor/fasipan/wofan/");

defined('FAN_TEXTDOMAIN') ? FAN_TEXTDOMAIN : define('FAN_TEXTDOMAIN', 'wofan');

/*----------------------------------------------------*/
// Storage path.
/*----------------------------------------------------*/
defined('WFAN_STORAGE') ? WFAN_STORAGE : define('WFAN_STORAGE', WP_CONTENT_DIR . DS . 'storage');

if (!function_exists('global_set_paths')) {
    /**
     * Register paths globally.
     *
     * @param array $paths Paths to register using alias => path pairs.
     */
    function global_set_paths(array $paths)
    {
        foreach ($paths as $name => $path) {
            if (!isset($GLOBALS['themosis.paths'][$name])) {
                $GLOBALS['themosis.paths'][$name] = realpath($path) . DS;
            }
        }
    }
}

if (!function_exists('global_path')) {
    /**
     * Helper function to retrieve a previously registered path.
     *
     * @param string $name The path name/alias. If none is provided, returns all registered paths.
     *
     * @return string|array
     */
    function global_path($name = '')
    {
        if (!empty($name)) {
            return $GLOBALS['themosis.paths'][$name];
        }

        return $GLOBALS['themosis.paths'];
    }
}
require_once PATH_FRAMEWORK . DS . "bootstrap/admin.php";
require_once PATH_FRAMEWORK . DS . "bootstrap/app.php";
require_once PATH_FRAMEWORK . DS . "bootstrap/http.php";

/**
 * add assets admin
 */

Asset::add('style-admin', 'css/admin-style.css', [], '1.0', true)->to('admin');
Asset::add('scripts-admin', 'js/scripts.js', [], '1.0', true)->to('admin');
Asset::add('smoothscroll-admin', 'js/smoothscroll.min.js', [], '1.0', true)->to('admin');
Asset::add('pace-admin', 'js/pace.min.js', [], '1.0', true)->to('admin');
Asset::add('pace-script-admin', 'js/pace-script.js', [], '1.0', true)->to('admin');

Asset::add('admin-script-admin', 'js/admin.js', [], '1.0', true)->to('admin');

Asset::add('login-css-admin', 'css/login.css', [], '1.0', true)->to('login');
Asset::add('login-js-admin', 'js/login.js', [], '1.0', true)->to('login');
